<?php
require_once "../class/Autoloader.php";
Autoloader::register();
use gdb\GamesDB;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ob_start();?>
<h1 class="games-title">Games</h1>
<?php
$db = new GamesDB();
$games = $db->getAllGames();

foreach ($games as $game) {
    $game->getHTML();
}
?>


<?php
$result = ob_get_clean();

Template::render($result);

?>

