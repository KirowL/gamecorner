<?php
require_once "../class/Autoloader.php";
Autoloader::register();


ob_start();?>

<div class="new-game-title">New game</div>

<?php
    \gdb\GameForm::generateForm();
?>

<?php
$result = ob_get_clean();

Template::render($result);

?>

