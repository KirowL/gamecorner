<?php
require_once "class/Autoloader.php";
require_once "class/gdb/GamesDB.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!empty([$_FILES["image"]])) {
    $image = $_FILES["image"];
}


if (!empty($_POST["name"])) {
    if ($image["error"] == 0) {
        $temp_name_file = $image["tmp_name"];
        $file_name = $image["name"];

        $dir_name = "./uploads/";
        if (!is_dir($dir_name)) mkdir($dir_name);

        $full_name = $dir_name . $file_name;
        move_uploaded_file($temp_name_file, $full_name);

        $db = new gdb\GamesDB();
        $db->createGame($_POST["name"], $_POST["description"], $file_name);
        header("Location: https://thomas-obein.fr/projects/Web3/TP5/pages/games_list.php");
    }
}