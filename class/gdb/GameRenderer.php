<?php

class GameRenderer
{
    public function getHTML() {?>
        <div class="game-container">
            <h1 class="game-title"><?=$this->name?></h1>
            <div class="game-infos">
                <img src=<?= "../uploads/" . $this->image ?> />
                <p class="game-descripton"><?= $this->description ?></p>
            </div>
        </div>
    <?php
    }
}