<?php
namespace gdb;
use PDO;

require_once "GameRenderer.php";
class GamesDB
{
    private PDO $pdo;

    public function __construct() {
        $db_name = "NOM_DB";
        $db_host = "HOST";
        $db_port = "PORT";

        $db_user = "USER";
        $db_pwd = "PASSWORD";

        try {
            $dsn = "mysql:dbname=" . $db_name . ";host=" . $db_host . ";port=" . $db_port;

            $this->pdo = new PDO($dsn, $db_user, $db_pwd);
        }
        catch (\Exception $ex) {
            die("Erreur: " . $ex->getMessage());
        }
    }

    public function getAllGames() {
        $query = "SELECT * FROM games";
        $statement = $this->pdo->prepare($query);

        $statement->execute() or die(var_dump($statement->errorInfo()));

        $result = $statement->fetchAll(PDO::FETCH_CLASS, "GameRenderer");

        return $result;
    }

    public function createGame($name, $description=null, $imgFile=null) {
        $query = "INSERT INTO games (name, description, image) VALUES (:name, :description, :image)";
        $statement = $this->pdo->prepare($query);

        $statement->bindValue(":name", $name);
        $statement->bindValue(":description", $description);
        $statement->bindValue(":image", $imgFile);
        $statement->execute() or die(var_dump($statement->errorInfo()));
    }
}