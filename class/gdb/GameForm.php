<?php

namespace gdb;

class GameForm
{
    public static function generateForm() {?>
        <form method="POST" action="../create_game.php" enctype="multipart/form-data">
            <div class="form-element">
                <label for="name">Name</label>
                <input type="text" name="name">
            </div>

            <div class="form-element">
                <label for="description">Description</label>
                <input type="text" name="description" maxlength="255">
            </div>

            <div class="form-element">
                <label for="image">Image</label>
                <input type="file" id="image" name="image" accept="image/png, image/gif, image/jpeg, image/jpg">
            </div>

            <div class="control-buttons">
                <button type="submit">Submit</button>
                <button type="reset">Reset</button>
            </div>
        </form>

    <?php
    }
}