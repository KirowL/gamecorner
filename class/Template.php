
<?php
class Template
{

    public static function render(string $content) {?>

        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="../styles/header.css">
            <link rel="stylesheet" href="../styles/footer.css">
            <link rel="stylesheet" href="../styles/style.css">
            <title>Game Corner</title>
        </head>
        <body>
        <?php include "header.php" ?>

        <div id="injected-content">
            <?php echo $content ?> <!-- INJECTION DU CONTENU -->
        </div>

        <?php include "footer.php" ?>

        </body>
        </html>

        <?php
    }

}
?>
